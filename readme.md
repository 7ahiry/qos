[MERCI !!!](https://docs.google.com/forms/d/1TGyFAr_iEQr8Z69A3_1owEf2kXIdCv6TtBus0jR0gLA/viewform?usp=send_form)

# Travaux pratiques de QoS Telecom Lille 1


Cette page contient les instructions et documents pour les Travaux pratiques de
Qualité de Service de la formation SPIRALE 2015.






## Rappel rapide sur la qualité de service.

La qualité de service peut se définir comme étant "l'ensemble des phénomènes pouvant
influencer les performances du service qui détermine le degré de satisfaction de l'utilisateur
de ce service". La QoS est appréhendée différemment selon l'acteur : client, opérateur de
service ou de réseau et le rôle : demandeur, fournisseur, consommateur. Si elle se résume
souvent à l'écoulement du trafic dans un réseau, beaucoup d'autres facteurs sont à prendre en
compte.

La gestion de la QoS, et sa garantie, ne peut doit pas se restreindre à une
technologie ou une partition de réseau mais doit être abordée dans son intégralité.
Elle doit être considérée de bout en bout même si elle est ensuite traitée par
partition pour faciliter sa mise en oeuvre.
La QoS est donc égale à la QoS rendue par la partition la plus
faible sur l'ensemble des partitions par lesquels transite le service.
Ainsi, il ne sert à rien de garantir un niveau de QoS excellent dans le réseau
de l'opérateur si la liaison Wifi du réseau domestique ne permet pas d'exploiter
cette qualité de service.

## Les métriques de la qualité de service.

La qualité de service sera exprimée dans un langage non technique comme pour les
carte bancaires ou les whisky (gold, silver, bronze,... Black, Red, Blue,.. )
pour et le plus permettre à l'utilisateur de mieux comprendre ce dont il s'agit.
A l'inverse, les métriques utilisés dans le réseau doivent de répondre à des
exigences techniques. Les métriques de QoS associés à un flux de données sont principalement :

- Le débit (bandwidth) du flux qui désigne la quantité d'informations utiles
écoulée par unité de temps exprimé en bits/s. Dans un réseau le débit peut se
mesurer à plusieurs niveau des couches OSI.

- Le taux de perte (loss rate et error rate) qui désigne la probabilité maximale de
perte de données ou de paquets.

- Le délai (delay) de transmission, exprimé en s, désigne le temps nécessaire pour
acheminer un bit ou paquet (unité atomique du flux) de la source jusqu'à la destination. Ce
paramètre peut correspondre à une valeur maximale à ne pas dépasser, une mesure
moyenne ou minimale ... mais en aucun cas ne désigne le temps total de transfert.

- La gigue (jitter ou delay variation) qui désigne la variation du délai de transmission,
exprimée en s.

Si le débit et le taux de perte concernent toutes les applications, le délai et la gigue
affectent plus particulièrement les applications à temps réel.

## Notes sur ce TP

__Ce TP se divise en  deux parties. Pour ces deux parties vous
devez rédiger un rapport de TP reprenant les choix que vous avez fait et expliquant
les résultats que vous avez eu. Pour chaque partie, le rapport contientra entre autres
(ceci n'est pas une liste exhaustive et encore moins un plan)__

__Comme discuté au début du TP, vous n'avez rien à rendre, je passerai pour évaluer
votre travail__

- Un résumé de ce que vous avez fait avec une idée des résultats obtenus
- Une description de l'environement, outils (même si je l'ai déjà fait), architecture
réseau
- une présentation des expérimentations que vous avez effectuées
- une présentation des résultats obtenus
- une conclusion
- des annexes qui peuvent être en ligne contenant du code, des scripts, les données
brutes obtenues.











# Installation de votre environement

Ne disposant pas d'un réseau complet, nous allons, enfin plutôt vous allez,
dans ce TP travailler sur un réseau simple pour bien comprendre et mettre en oeuvre de la
QoS sous linux en utilisant des principes de bases de la QoS.

L'objectif de ce TP est de mettre en oeuvre de la qualité de service sous linux
et de montrer par des graphes que cette qualité de service a du sens ( fonctionne
 et sert ).  L'idée est simple, donner de la priorité à certain flux, plus
 qu'a d'autres.



## Le réseau

Vous pouvez mettre en place un réseau de votre choix, ci dessous, deux exemples
que vous pouvez exploiter, mais vous pouvez faire de plus grand réseaux ou plus
complexes.


### Option 1
```
 +--------+                         +--------+
 |computer|      network            |computer|
 |        +-------------------------+        |
 |   1    |             link        |   2    |
 +--------+                         +--------+
```

### Option 2
```
 +----------+                       +----------+
 | COMPUTER |                       | COMPUTER |
 |          +----------  -----------+          |
 |    1     |         |  |          |    2     |
 +----------+         |  |          +----------+
                   +--+--+--+
                   | SWITCH |
                   +---+----+
                       |
                  +----+-----+
                  |  SERVER  |
                  |          |
                  |          |
                  +----------+
```

Dans ce réseau, le(s) lien(s) peut(peuvent) être sans fil et/ou filaire. L'outils
[netem](http://www.linuxfoundation.org/collaborate/workgroups/networking/netem)
permet de modifier les propriétés du lien. Avec un peu d'avance, l'outil TC est
introduit. Vous utiliserez cet outil plus tard. Je n'ai pas testé la commande
`tc` avec les options `netem`. En fin de ce document, vous avez les informations 
et les commandes `tc` qui vont bien.





### Décrire votre réseau

Décrivez votre réseau le ou les liens vos systemes d'exploitation, etc... enfin
toute information utile pour bien comprendre et refaire votre TP.




## Outils pour le trafic

__Les deux parties du TP se trouvent ici. dans la première partie vous devez utiliser
des générateurs de trafic fictif. Dans la seconde partie, vous utilisez du trafic
réel (en tout cas générer par des applications).__

### Générer du trafic fictif

Pour générer du trafic, il y a plusieurs manière de faire et il existe sous
linux plusieurs outils pour le faire dont :

- [hping3](http://hping.org)
- [Ostinato](http://ostinato.org/)

Mes préférés et ceux que je trouve les plus facile à utiliser et à comprendre sont:
- [iperf](https://iperf.fr/) avec un tutoriel rapide [ici](http://blog.nicolargo.com/2008/01/iperf-des-exemples.html)
- [mgen](http://downloads.pf.itd.nrl.navy.mil/docs/mgen/mgen.html) couplé avec [trpr](http://downloads.pf.itd.nrl.navy.mil/docs/proteantools/trpr.html).
Pour plus d'info sur l'utilisation de `mgen` voir plus bas.


### Générer du trafic réel

Une autre option pour générer du trafic est d'installer des applications
et de les utiliser.

- __Du trafic web__

Vous un trqfic web, il vous faut installer un serveur web d'un côté et un navigateur
ou un outils permettant de simuler des accès à ce serveur. Utiliser les outils de votre
choix.

Les outils que je connais sont:
* un tout petit serveut http comme [mini-httpd](http://manpages.ubuntu.com/manpages/natty/man8/mini-httpd.8.html) par exemple.
* un outils pour stresser votre serveur http: [siege](https://www.joedog.org/siege-manual/).

Il existe un nombre phénoménal de serveur web plus ou moins petit, plus ou moins facile
à utiliser. Vous pouvez choisir celui qui vous semble le plus adapté.



- __Du trafic FTP__

Pour générer du trafic FTP, vous pouvez installer un serveur FTP, comme
[vsftpd](http://linux.developpez.com/vsftpd/) et le configurer pour pouvoir
effectuer des téléchargements. Vous pouvez bien sur utiliser n'importe quel
serveur FTP.

Sur le poste client, vous pouvez installer Filezilla(https://filezilla-project.org/) ou tout
autre client avec lequel vous vous sentez à l'aise.


- __Du streaming vidéo__

Pour le streaming video, vous pouvez utiliser l'outils [VLC](http://www.videolan.org/vlc/).
Pour le streaming, vous pouvez utiliser [ce fichier](http://7ahiry.github.io/TedxTalk.avi) par exemple.

Je ne connais pas d'autre outils pour faire du streaming vidéo facilement. Je décris
dans le paragraphe qui suit ce que j'entends par facilement.

Pour lancer un streaming avec `VLC` sur la machine serveur, lancer VLC, File>Stream.
Chercher le fichier à diffuser. Choisissez HTTP et le port de diffusion. Choisir
un mode de diffusion H323 ou autre. Diffuser.

Sur la machine cliente, dqsn `VLC` file>Open network stream et renseigner les informations de
diffusion adresse IP du serveur et port de diffusion.



### Analyser le trafic
Dans tous les cas pour analyser le trafic il vous faudra utiliser au choix:
- [wireshark](http://www.wireshark.org) avec un tutoriel simple [ici](http://openmaniak.com/fr/wireshark_filters.php)
et [ici](http://www.howtogeek.com/104278/how-to-use-wireshark-to-capture-filter-and-inspect-packets/)
- [tcpdump](http://www.tcpdump.org/tcpdump_man.html) avec des tutoriels [ici](http://openmaniak.com/fr/tcpdump.php)
et [ici](https://danielmiessler.com/study/tcpdump/)
- [trpr](http://downloads.pf.itd.nrl.navy.mil/docs/proteantools/trpr.html). Pour
plus d'infos sur l'ulisation de `trpr` voir plus bas.
- [tcptrace](http://www.tcptrace.org/index.html) pour traiter les sorties de `tcpdump` par exemple.



### Décrire vos outils

Vous pouvez utiliser n'importe quel outil pour générer et/ou analyser le trafic.
Dans tous les cas, vous devez décrire ces outils, les scripts que vous utiliser
et essayer de justifer vos choix. Il faut un description de telle sorte que
quelqu'un puisse suivre toute les démarches que vous avez effectué pour refaire
votre travail.





## Outils pour la QoS

L’objectif est de mettre en place un système de contrôle du trafic sortant d’un
serveur pour ne pas saturer le lien et donner une priorité à certain type de flux
en sortie de ce serveur.

Pour ce faire, les outils dont vous disposez sous linux sont entre autres
(vous n'avez pas le choix ici). Pour activer les options de QoS dans le noyau linux:

```
$ sudo modprobe sch_cbq sch_sfq sch_htb
```

Les outils dont vous disposez sous linux sont décrits dans les sous-sections suivantes.


### IP Route
Elle permet de faire de ce qui est fait par `ifconfig` et `route`. La commande
`ip` peut servir à :

```
// pour configurer les liens réseaux sur une machine
ip link ...
```
```
// pour configurer les adresses IP sur une machine
ip addr 172.16.11.1/24 broadcast 172.16.11.255 dev eth0
```

```
// pour configuer les tables de routage sur une machine
ip route add ...
```

Pour se familiariser avec les commandes consulter la documentation
[”IP Command Reference”](http://linux-ip.net/gl/ip-cref/).


### Découvrir les commandes de contrôle de trafic : `tc`

Cette commande permet de créer une hiérarchie de files d’attentes permettant
de différencier les flux, d'associer des classes à ces files d'attente qui
représenteront des traitements génériques, et d'associer des
filtres permettant déorienter les paquets vers telle ou telle file d’attente
de telle ou telle classe.

- `tc qdisc ...` // pour créer des disciplines de mise en file d’attente
- `tc class ...` // association d’une classe à une discipline de mise en file d’attente
- `tc filter ...` // description de la m´ethode de filtrage pour classer les paquets



### Le marquage des paquets avec : `iptables`

`iptables` permet de marquer les paquets pour pouvoir les classifier dans la bonne
file d'attente.

```
iptables -t mangle -A OUTPUT -p tcp --dport 2049 -j MARK --set-mark 0x1
```

Normalement cette commande permet de mettre une étiquette 1 à tous les paquets
tcp dont le port de destination est 2049. Il est possible de marquet directement
les paquets avec la commande `tc` voir [ici](http://www.inetdoc.net/guides/lartc/lartc.qdisc.filters.html#idp49924608).




# Tout mettre ensemble

Vous devez maintenant mettre en place de la Qualité de service au niveau de
votre serveur pour permettre de profiter au mieux de votre réseau. L'idée est
de donner une certaine priorité à votre trafic. À vous de choisir. Utiliser le
filtrage les discipline de file d'attente, la limitation de débit, le lissage
 de trafic etc.

Utiliser les commande et la configuration ci-dessus ou une configuration de
votre choix. En utilisant les commandes `ip`, `tc`, `iptables`, mettez en place
de qualité de service sur votre réseau.

- Faîtes une première série d'expérience avec du trafic fictif.
- Faîtes une seconde série d'expérience avec du trafic réel.

Dans les deux cas précédents, montrez le gain. La différence entre un réseau
sans QoS et un réseau avec QoS. Donnez les scripts d'analyse que vous avez créés
et mettez en ligne les données brutes.

__Le gros de votre travail c'est de comprendre l'outils `tc` et son utilisation; Donc
le gros de votre travail va être de la lecture__

Vous pouvez me rendre le rapport pour le __27 Novembre 2015__





# NOTES sur les OUTILS de GENERATION DE TRAFIC

__TEST 1: Test et génération de trafic UDP/TCP simple__

Pour génerer du trafic :
    - UDP
    - TCP

Vous pouvez utiliser les outils
[mgen](http://downloads.pf.itd.nrl.navy.mil/docs/mgen/mgen.html) pour générer du
trafic et [trpr](http://downloads.pf.itd.nrl.navy.mil/docs/proteantools/trpr.html)
pour visionner.

Installation des outils:
```
sudo apt-get install gnuplot wireshark libpcap-dev iproute siege vlc vsftpd mini-httpd
```
Gnuplot est un logiciel qui permet de tracer les courbes, wireshark est un snifer
de paquet, libpcap est une librairie permettant de gérer les paquet tcpdump et
iproute contient les outils tels que `ip` et `tc`.

Pour mgen

```
wget http://downloads.pf.itd.nrl.navy.mil/mgen/src-mgen-5.02b.tar.gz
tar xvf src-mgen-5.02b.tar.gz
cd src-mgen-5.02b/makefiles
make -f Makefile.linux
sudo cp mgen mpmgr /usr/bin/.
```


Pour TRPR

```
wget http://downloads.pf.itd.nrl.navy.mil/proteantools/src-trpr-2.1b2.tgz
tar xvf src-trpr-2.1b2.tgz
cd TRPR
g++ -w -o trpr trpr.cpp -lm
sudo cp trpr /usr/bin/.
```

L'outil [trpr](http://downloads.pf.itd.nrl.navy.mil/docs/proteantools/trpr.html)
doit vous permettre d'obtenir les mesures de QoS quand vous utilisez
[mgen](http://downloads.pf.itd.nrl.navy.mil/docs/mgen/mgen.html)

Pour info. `mgen` se lance par la commande : `mgen input <scriptfile> [output<logfile>]`
le fichier scriptfile est un fichier qui comporte les indications sur la forme du trafic
le temps, les ports et addresses IP de destination.

Un exemple de script mgen d'émission:
```
#Démarrer (instant0.0) un flux numéro 1 MGEN envoyant des messages UDP de 1024 octets
#sur le port 5000 de l'adresse IP locale (machine locale)
#a un débit moyen de 10.0 octets par seconde, selon une loi de Poisson
#L'emission du flux numéro 1 s'arrete au temps 500.0
0.0 ON 1 UDP DST 127.0.0.1/5000 POISSON [10.0 1024]
500.0 OFF 1
```

Vous pouvez générer plusieurs type de trafic, je veux dire autre que poisson.
Testez par exemple PERIODIC.

Un exemple de script mgen d'émission:
```
0.0 LISTEN UDP 5000
```

Recupérer les données de reception dans un fichier. En supposant que votre
script de réception s'appelle `recv.mgen`, on redirige la sortie vers un fichier
`/tmp/rawdata`

```
mgen input recv.mgen > /tmp/rawdata.mgen
```

On traite ce fichier avec `trpr`. Les commandes suivantes fonctionnent:

```
trpr input /tmp/rawdata.mgen history 500 window 1 mgen real > debit.raw
```

ici, history permet de specifier le temps (axe des x) et window permet de spécifier
les points sur cet axe des x, ici toutes les seconds (je crois). On redirige la
sortie vers debit.dat. Vous avez les options suivantes pour la commande précédente:

```
trpr input rawdata history 500 window 1 mgen real loss > loss.raw
trpr input rawdata history 500 window 1 mgen real interarrival > interarrival.raw
```

Normalement, les fichiers `*.raw` générer doivent pouvoir être traités directement
avec gnuplot mais je n'ai pas réussi. Il faut éditer le fichier en 2 fichiers.

NOTE: la deuxième commande permet d'avoir les temps inter-arrivé des paquets. Pour
avoir la gigue il faut faire un petit calcul.

```
trpr input rawdata history 500 window 1 real loss > loss.raw
trpr input rawdata history 500 window 1 real interarrival > interarrival.raw
```



Un fichier debit.gnu qui contient:
```
set terminal pngcairo size 350,262 enhanced font 'Verdana,10'
set output 'debit.png'
set xlabel 'Time (sec)'
set ylabel 'Rate (kbps)'
set style data lines
set yrange[0.000000:*]
set key bottom right
set xrange[0.000000:120.000000]
plot 'debit.dat' w lp t 'mgen,1 @IP SRC->@IP SRC/port~1'
```

A quelques détails près ces lignes sont dans votre fichier `*.raw`

et un Fichier `debit.dat`

```
0.000000, 8306.688000
1.000000, 8306.688000
2.000000, 8085.504000
...
...
120.000000, 8005.504000
```

A quelques détails près ces lignes sont dans votre fichier `*.raw`

La commande `gnuplot debit.gnu` devrait vous générer un fichier `debit.png`
qui contient un plus ou moins joli graphe. Consultez les pages de [gnuplot](http://www.gnuplot.info/)
pour avoir des graphes vraiment jolis avec un aspect un peu plus professionnel.

Vous pouvez automatiser tout ce processus... avec un gros script bash/awk/python
comme vous voulez. Faîtes-le vous gagnerez du temps. Enfin je crois.

Je vous recommande d'utiliser `tcpdump` pour capturer le trafic générer par `mgen`
et d'analyser ce trafic avec `trpr`.


__TEST 2: Test et génération de trafic HTTP__

Installer un serveur http comme [mini-httpd](http://manpages.ubuntu.com/manpages/natty/man8/mini-httpd.8.html) par exemple.

Pour stresser votre serveur http, utiliser l'outil [siege](https://www.joedog.org/siege-manual/).
Cet outil est une commande qui génère des requêtes http à un serveur. L'intérêt
de cet outil est qu'il permet de générer ces requêtes de façon
concurrente (i.e. en parallèle) et donc de « stresser » votre serveur.

Le trafic HTTP va surtout nous servir de trafic de background. Il n'est pas important de
receuillir les informations de performance sur ce trafic. (enfin je crois)

Dans un répertoire du serveur, créez un fichier `index.html` plus ou moins gros avec plus
ou moins d'images. Comme vous voulez en fait. Sur le serveur lancez (et dans ce
répertoire):

```
sudo mini-httpd
```

Côté client, lancé un navigateur pour tester si tout fonctionne. Normalement,
les pages sont servies sur le port 80. `mini-httpd -p 8080` pour les servir
sur le port 8080. Voir page du manuel de `mini-httpd` car je ne suis pas certain.

```
 siege -c 800 http://localhost:8080
```

cette commande simule 800 requêtes simultanées, attends une seconde, puis relance 800 requêtes
et ainsi de suite. Laissez tourner le programme quelques secondes (20 tout au plus) et arrêtez
le à l'aide de Ctrl+C.

Utiliser [`tcpdump`](http://www.tcpdump.org/tcpdump_man.html) avec l'option `-X`
pour capturer le trafic et rediriger vers une interface:

```
tcpdump -S -n -X -p -i eth0 -w <tracefile> --time-stamp-precision nano -tt
```

Il y a un tutoriel rapide sur `tcpdump` [ici](http://openmaniak.com/fr/tcpdump.php)
et un autre [ici](https://danielmiessler.com/study/tcpdump/)
il faut vérifier que `tcpdump` supporte l'option `--time-stamp-precision nano`. L'option
`-tt` permet d'afficher les timestamps unix au lieu de la date.


Normalement le ficher genérer peut être traîter avec `trpr` avec la commande suivante:

```
trpr input <traceFile> auto X output <plotFile>
```

Le fichier plotFile doit être traîter comme précédemment, c'est à dire diviser en deux.



__TEST 3: Test et génération de trafic FTP__

Pour générer du trafic FTP, installer le serveur FTP, comme
[vsftpd](http://linux.developpez.com/vsftpd/) et le configurer pour pouvoir
effectuer des téléchargements. Vous pouvez bien sur utiliser n'importe quel
serveur FTP.

Sur le poste client, installez Filezilla(https://filezilla-project.org/) ou tout
autre client avec lequel vous vous sentez à l'aise.


Utiliser [`tcpdump`](http://www.tcpdump.org/tcpdump_man.html)


__TEST 4: Test et génération de trafic streaming video__

Pour le streaming video, utiliser l'outils [VLC](http://www.videolan.org/vlc/).
Pour le streaming, utiliser [ce fichier](http://7ahiry.github.io/TedxTalk.avi).
Pour lancer un streaming avec VLC sur la machine serveur, lancer VLC, File>Stream.
Chercher le fichier à diffuser. Choisissez HTTP et le port de diffusion. Choisir
un mode de diffusion H323 ou autre. Diffuser.

Sur la machine cliente, file>Open network stream et renseigner les informations de
diffusion adresse IP du serveur et port de diffusion.

Utiliser [`tcpdump`](http://www.tcpdump.org/tcpdump_man.html)


# NOTES sur les OUTILS de QoS

Vous trouvez sur internet une foule de document concernant l'implémentation de
la QoS sous linux.

- Ressource d'un ancien prof  à moi [ici](http://cpham.perso.univ-pau.fr/ENSEIGNEMENT/QOS/QOS.html)
- [Linux Advanced Routing & Traffic Control](http://www.inetdoc.net/guides/lartc/)
- [QOS sous Linux avec TC](http://wiki.linuxwall.info/doku.php/fr:ressources:dossiers:networking:qos_traffic_control)
- [QoS et gestion du trafic avec Traffic Control](http://connect.ed-diamond.com/GNU-Linux-Magazine/GLMF-127/QoS-et-gestion-du-trafic-avec-Traffic-Control)
- [HTB Linux queuing discipline manual - user guide](http://luxik.cdi.cz/~devik/qos/htb/manual/userg.htm)
Il en existe plein d'autre.


# Extensions

Il y a plusieurs extension à ce TP. On pourrait faire
- Du load balancing
- On peut faire du contrôle d'admission



# Notes

Si vous renconter un problème, merci d'ouvrir une issue sur ce dépot.

## Actions préliminaires

Si vous n'êtes pas familier avec le système de contrôle de version git vous
pouvez lire ce petit [tutorial](http://learnxinyminutes.com/docs/git/) ou
[celui-ci](http://7ahiry.github.io/tools/2015/06/04/my-git-tutorial.html).

Vous pouvez vous créer un compte sur [https://git.framasoft.org](https://git.framasoft.org)
ce dépot est gratuit et basé sur un logiciel open source : [gitlab](http://gitlab.com)

## Autres outils

Le commande suivante permet de convertir une date heure en un format plus facile à
manipuler pour le calcul
```
$ date -d 17:41:20.319103114 +%H%M%S%N
$ 174120319103114
```

`tcpdump` permet de rejouer une trace capturer. Le point fort et que vous pouvez
appliquer des filtres sur cette trace pour le regarder qu'un type de trafic
par exemple.

```
tcpdump -r test.dump --time-stamp-precision nano -tt src 193.49.212.76
```

rejoue la trace `test.dump` en ne regardant que les paquets dont la source
est `193.49.212.76`

### Interfaces virtuelles

Pour avoir plusieurs IP sur une même interface.

```
sudo ifconfig eth0:0 192.168.1.11 up
```

### Example très basique pour TC. 

#### Pour voir les règles `tc` existantes sur `eth0`
```
$ tc -s qdisc ls dev eth0
```

La sortie ressemble à:
```
qdisc pfifo_fast 0: root bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
 Sent 2732108 bytes 10732 pkt (dropped 0, overlimits 0 requeues 0)
 rate 0bit 0pps backlog 0b 0p requeues 0
```

#### Premier test de trafic shapping

Faire un 
```
ping de 8.8.8.8
```

Regarder le résultat. Maintenant ajouter un delai de 500ms
```
tc qdisc add dev eth0 root netem delay 500ms
```

Rrelancer le ping et comparer les résultats avec les résultats précédents.
Lister les règles:

```
tc -s qdisc ls dev eth0
```

Vous devriez voir:
```
qdisc netem 8001: root limit 1000 delay 500.0ms
 Sent 175545 bytes 540 pkt (dropped 0, overlimits 0 requeues 0)
 rate 0bit 0pps backlog 0b 0p requeues 0 
```

Supprimer toute les règles:
```
$ tc qdisc del dev eth0 root
$ tc -s qdisc ls dev eth0
```
D'autres commandes intéressantes pour le traffic sont:
```
$ tc qdisc change dev eth0 root netem loss 0.1% # perte de paquet
$ tc qdisc change dev eth0 root netem loss 0.3% 25% # perte de paquet avec proba conditionnelles
$ tc qdisc change dev eth0 root netem duplicate 1% # duplication de paquet
$ tc qdisc change dev eth0 root netem corrupt 0.1% # corruption de paquet
$ tc qdisc change dev eth0 root netem delay 100ms 10ms # ajoute un alea de plus ou mois 10% pour le délai
$ tc qdisc change dev eth0 root netem delay 100ms 10ms 25% # ajoute un alea de plus ou mois 10% pour le délai avec correlation
$ tc qdisc change dev eth0 root netem delay 100ms 20ms distribution normal # delai suivant une distribution normale - normal, pareto, paretonormal -
```
Il faut utiliser `tc qdisc add` quand l'interface n'a pas de règle et `tc qdisc change` si l'interface a deja une règle.
Si vous utiliser `tc qdisc change` alors qu'il n'y a pas de règle vous obtiendrai une erreur: `RTNETLINK answers: No such file or directory`


Les pertes peuvent provoquer des comportements bizarre au niveau de la 
retransmission avec TCP. Il faut faire attention.

#### Modification du débit sortant (rate control)

`netem` ne possède pas de moyen de limiter le débit. Pour cela on peut par exemple
utiliser un filtre token bucket (TBF : Token Bucket Filter).

```
tc qdisc add dev eth0 root tbf rate 1mbit burst 10kb latency 70ms peakrate 2mbit minburst 1540
```

Cette commande permet d'avoir un débit maximum de 1mbits/s des pics à 2mbit et un délai du token bucket de 70ms.


#### Contrôler le traffic sortant

Pour contrôler le traffic sortant sur un lien, on utilise HTB (HTB - Hierarchy Token Bucket).

1. rate - Permet de définir le débit d'arriver des token.
1. ceil - Permet de définir et de contrôler les burst, plus ou moins la taille du sceau à jeton.
1. prio - Permet de gérer les priorités pour le reste du débit. Les classes avec les 
petites priorités ont plus de débit. 
1. iptables and tc: Permet de controler le trafic.

__Un exemple avec HTTP (en sortie)__

Supprimer les règles:
```
$ tc qdisc del dev eth0 root
```

Mettre en place la discpline de file d'attente:
```
$ tc qdisc add dev eth0 root handle 1:0 htb default 10
```

Définir la classe et les limitations
```
$ tc class add dev eth0 parent 1:0 classid 1:10 htb rate 512kbps ceil 640kbps prio 0
```

Utiliser iptable pour filter sur le port 80 et marquer les paquets

```
$ iptables -A OUTPUT -t mangle -p tcp --sport 80 -j MARK --set-mark 10
```

Filter en envoyant dans la bonne file d'attente les paquets marqués:

```
$ tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 10 fw flowid 1:10
```

Pour voir ce que vous avez fait
```
$ tc -s qdisc ls dev eth0
$ tc -s filter show dev eth0 parent ffff:
$ tc -s -d class show dev eth0
```

___Exemple pour le port 80 et 22___
```
$ tc qdisc add dev eth0 root handle 1: htb
$ tc class add dev eth0 parent 1: classid 1:1 htb rate 1024kbps
$ tc class add dev eth0 parent 1:1 classid 1:5 htb rate 512kbps ceil 640kbps prio 1
$ tc class add dev eth0 parent 1:1 classid 1:6 htb rate 100kbps ceil 160kbps prio 0
$ tc filter add dev eth0 parent 1:0 prio 1 protocol ip handle 5 fw flowid 1:5
$ tc filter add dev eth0 parent 1:0 prio 0 protocol ip handle 6 fw flowid 1:6
$ iptables -A OUTPUT -t mangle -p tcp --sport 80 -j MARK --set-mark 5
$ iptables -A OUTPUT -t mangle -p tcp --sport 22 -j MARK --set-mark 6
```

___Quelques commandes pour faire un monitoring rapide___
```
$ tc -s -d class show dev eth0
$ iptables -t mangle -n -v -L
$  iptraf
$  watch tc -s -d class show dev eth0
```

___Simuler un lien adsl___

Pour simuler un lien ADSL, vous pouver utiliser les parametres suivant:

UPLOAD
```
Débit sortant: BWU=768kbit
Délai de transit sortant : DELAYU=20ms
Paquets perdus sortant : LOSSU=0.01%
```

DOWNLOAD
```
Débit entrant : BWD=2mbit
Délai de transit entrant : DELAYD=20ms
Paquets perdus entrant : LOSSD=0.01%
```

Vous pouvez l'effectuer sur deux PC ou avec les commandes `netem` pour les lien sortant pour les deux PC.
Ou sur un seul pc en appliquant les délais/perte/débit en entrée avec `ifb`. Je crois qu'il faut un `modprobe ifb`